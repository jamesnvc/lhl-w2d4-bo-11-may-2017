//
//  ViewController.m
//  GesturesBreakout
//
//  Created by James Cash on 11-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "ColorThing.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *colourView;

// model step 1: make property for it
@property (strong, nonatomic) ColorThing *colorModel;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchRecog;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(moveColourView:)];
    // when you see a long press, do [self moveColourView:...]
    // if you had [... target:foobar action:@selector(quux:)]
    // that would do [foobar quux:..];

    // aside: ignore this if you don't care
    // equivalent with blocks ^{ [self moveColourView:...] }


    // next, we need to actually tell the recognizer which view to be looking for the gesture in
    [self.colourView addGestureRecognizer:longPress];

    // in this case, we don't need this, but if this were a UILabel or UIImageView, we need to tell it we actually want this view to get touch events
    self.colourView.userInteractionEnabled = YES;


    // Data model
    // model step 2: create an instance of the model
    self.colorModel = [[ColorThing alloc] init];

    [self.pinchRecog addTarget:self action:@selector(changeColorScale:)];
    [self.colourView addGestureRecognizer:self.pinchRecog];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)moveColourView:(UILongPressGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.colourView.backgroundColor = [UIColor redColor];
    }
    if (sender.state == UIGestureRecognizerStateChanged) {
        self.colourView.center = [sender locationInView:self.view];
    }
    if (sender.state == UIGestureRecognizerStateEnded) {
        self.colourView.backgroundColor = [UIColor greenColor];
    }
}

- (IBAction)changeColor:(UIPanGestureRecognizer *)sender {
    CGPoint movement = [sender translationInView:self.view];
    NSLog(@"%@", NSStringFromCGPoint(movement));
    // resetting translation so we get movement between the last time this was called & now
    // otherwise it would be cumulative for this entire gesture
    [sender setTranslation:CGPointZero inView:self.view];
    // use pythagoras to get a scalar from the CGPoint
    CGFloat disp = sqrtf(movement.x*movement.x + movement.y*movement.y);
    // model step 3.1: notify model of information from gesture event
    [self.colorModel changeBasedOnDirection:disp];

    CGPoint velocity = [sender velocityInView:self.view];
    CGFloat speed = sqrtf(velocity.x*velocity.x + velocity.y*velocity.y);
    // model step 3.2: notify model of information from gesture event
    [self.colorModel changeBasedOnSpeed:speed];

    // model step 4: update view based on model state
    self.colourView.backgroundColor = self.colorModel.currentColor;
}

- (void)changeColorScale:(UIPinchGestureRecognizer*)sender {
    self.colourView.transform = CGAffineTransformMakeScale(sender.scale, sender.scale);
}

@end
