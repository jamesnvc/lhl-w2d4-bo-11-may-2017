//
//  ColorThing.h
//  GesturesBreakout
//
//  Created by James Cash on 11-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorThing : NSObject

@property (nonatomic,strong) UIColor *currentColor;

- (void)changeBasedOnSpeed:(CGFloat)speed;
- (void)changeBasedOnDirection:(CGFloat)direction;

@end
