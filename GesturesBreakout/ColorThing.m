//
//  ColorThing.m
//  GesturesBreakout
//
//  Created by James Cash on 11-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ColorThing.h"

@implementation ColorThing

- (instancetype)init
{
    self = [super init];
    if (self) {
        _currentColor = [UIColor greenColor];
    }
    return self;
}

- (void)changeBasedOnDirection:(CGFloat)direction
{
    CGFloat h, s, b, a;
    [self.currentColor getHue:&h saturation:&s brightness:&b alpha:&a];

    CGFloat newHue = fabs(direction / 100.0);
    self.currentColor = [UIColor colorWithHue:newHue saturation:s brightness:b alpha:a];
}

- (void)changeBasedOnSpeed:(CGFloat)speed
{
    CGFloat h, s, b, a;
    [self.currentColor getHue:&h saturation:&s brightness:&b alpha:&a];

    CGFloat newSat = fabs(speed / 100.0);
    self.currentColor = [UIColor colorWithHue:h saturation:newSat brightness:b alpha:a];
}

@end
